# Makefile for top-level directory

CONF_DEFAULT = gcc_opt

ifeq ($(CONF),)
  $(info CONF unset. Defaulting to 'CONF=$(CONF_DEFAULT)'.)
  $(info )
  CONF = $(CONF_DEFAULT)
endif

BINDIR = bin/$(CONF)
OBJDIR = obj/$(CONF)

.PHONY: all
all: obj src
	@( cd $(OBJDIR); \
	$(MAKE) -f ../../src/Makefile CONF=$(CONF) all \
	)

.PHONY: install
install: bin obj all
	@( for x in `ls $(OBJDIR)/*.x`; do cp $$x $(BINDIR); done )
	@echo Copied executables from $(OBJDIR) to $(BINDIR).
	@( cd bin; for x in `ls $(CONF)/*.x`; do ln -sf $$x .; done )
	@echo Linked executables from $(BINDIR) to bin directory.

.PHONY: bin
bin:
	@( if [ ! -d $(BINDIR) ]; then mkdir -p $(BINDIR); fi )

.PHONY: obj
obj:
	@( if [ ! -d $(OBJDIR) ]; then mkdir -p $(OBJDIR); fi )

.PHONY: clean
clean: obj
	cd $(OBJDIR); \
	rm -f *.o *.mod *.x

.PHONY: fullclean
fullclean: obj bin
	cd obj; \
	rm -f */*.o */*.mod */*.x; \
	cd ../bin; \
	rm -f */*.x

.PHONY: check
check: tests
	cd tests; \
	./run_tests.sh

# This is used to rebuild the tags list for use with exuberant ctags.
.PHONY: ctags
ctags:
	cd src; \
	ctags --fortran-kinds=+i *.f90

.PHONY: help
help:
	@echo
	@echo ' prog - fortran template project'
	@echo
	@echo ' To compile using e.g the configuration in config/gcc_opt type:'
	@echo ' > make CONF=gcc_opt'
	@echo ' This would create the object files and executables in the'
	@echo ' directory obj/gcc_opt.'
