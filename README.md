Fortran Template
================

This is a few sample Fortran source files and associated Makefiles that can be
used as a starting point for a larger project. The sample code uses modern
Fortran, including several items from the 2003 standard, so you may run into
issues if you need to use an old compiler.

A number of potentially useful functions and subroutines are included in the
provided modules, such as an overloaded multidimensional checked array
allocation routine.

Requirements
------------

The dependence on external tools is kept deliberately minimal. The example
code provided needs:
- a modern Fortran compiler, such as gfortran, supporting the 2003 standard.
- GNU Make
- The provided test script (which doesn't do anything except run the code,
  which also doesn't do anything) is written in bash.

Code Style
----------

A question that often comes up is what coding/formatting style is suggested.
I generally try to follow http://fortran90.org/src/best-practices.html with
the following exceptions/additions:

- Don't let any lines exceed 78 characters in length (so it's clear that lines
  finish within the width of an 80 character wide terminal). The only
  exception is the line continuation character & which can be at position 79.
- Indentation is 2 spaces. This is usually enough for clarity, and makes it
  easier to deal with the previous point.
- Use underscores in variable names (`snake_case`), `CamelCase` for
  subroutines and functions, `ALLCAPS` for external calls (to e.g. LAPACK
  subroutines).
- Space around most binary operations: `2 + 2 = 4`, but `3**2 = 9`
- Space following a comma.
- No space following an open parenthesis or bracket.
- Fortran 2003 style array declarations: `r = [1, 2, 3, 4]`
- Specify the intent for all arguments to subroutines and functions.
- A comment block should follow the begin line of subroutines and functions
  giving details of what the routine does.
- A further comment block should usually follow this giving details of all
  input and output variables.
- Never put a comment following a line continuation character.
- Do array assignments as e.g. `mat(:, :) = 0.0_dp` rather than `mat = 0.0_dp`
  so that it's clear that an array is being used.
- Use `forall` and `do concurrent` where execution order doesn't matter.
- Pure functions and subroutines where possible.
- Assumed shaped arrays are encouraged.
- Try to make the code intrinsically clear. Put paper references and formulas
  in the comments where appropriate.
