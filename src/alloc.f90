module alloc

! Array allocation routines

!------------------------------------------------------------------------------

  use consts

  implicit none

  interface CheckedAlloc

    ! If it is not already allocated, allocate an array, testing to see if it
    ! allocates successfully, and output an error message and exit the code if
    ! not. This is an interface to subroutines allocating arrays of dimensions
    ! 1 through 7, for both real(dp) and integer. It can be extended to
    ! more dimensions and types as necessary.

    module procedure CheckedAllocR1D
    module procedure CheckedAllocR2D
    module procedure CheckedAllocR3D
    module procedure CheckedAllocR4D
    module procedure CheckedAllocR5D
    module procedure CheckedAllocR6D
    module procedure CheckedAllocR7D
    module procedure CheckedAllocI1D
    module procedure CheckedAllocI2D
    module procedure CheckedAllocI3D
    module procedure CheckedAllocI4D
    module procedure CheckedAllocI5D
    module procedure CheckedAllocI6D
    module procedure CheckedAllocI7D

  end interface CheckedAlloc

  private
  public CheckedAlloc

  contains

!------------------------------------------------------------------------------

subroutine CheckedAllocR1D(dims, label, rarray)

  ! Allocate a 1D real array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   rarray - the allocated real array.

  integer, intent(in) :: dims(1)
  character(len=max_string_size), intent(in) :: label
  real(dp), intent(inout), allocatable :: rarray(:)

  integer :: alloc_err

  if (allocated(rarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(rarray(dims(1)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocR1D

!------------------------------------------------------------------------------

subroutine CheckedAllocR2D(dims, label, rarray)

  ! Allocate a 2D real array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   rarray - the allocated real array.

  integer, intent(in) :: dims(2)
  character(len=max_string_size), intent(in) :: label
  real(dp), intent(inout), allocatable :: rarray(:, :)

  integer :: alloc_err

  if (allocated(rarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(rarray(dims(1), dims(2)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocR2D

!------------------------------------------------------------------------------

subroutine CheckedAllocR3D(dims, label, rarray)

  ! Allocate a 3D real array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   rarray - the allocated real array.

  integer, intent(in) :: dims(3)
  character(len=max_string_size), intent(in) :: label
  real(dp), intent(inout), allocatable :: rarray(:, :, :)

  integer :: alloc_err

  if (allocated(rarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(rarray(dims(1), dims(2), dims(3)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocR3D

!------------------------------------------------------------------------------

subroutine CheckedAllocR4D(dims, label, rarray)

  ! Allocate a 4D real array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   rarray - the allocated real array.

  integer, intent(in) :: dims(4)
  character(len=max_string_size), intent(in) :: label
  real(dp), intent(inout), allocatable :: rarray(:, :, :, :)

  integer :: alloc_err

  if (allocated(rarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(rarray(dims(1), dims(2), dims(3), dims(4)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocR4D

!------------------------------------------------------------------------------

subroutine CheckedAllocR5D(dims, label, rarray)

  ! Allocate a 5D real array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   rarray - the allocated real array.

  integer, intent(in) :: dims(5)
  character(len=max_string_size), intent(in) :: label
  real(dp), intent(inout), allocatable :: rarray(:, :, :, :, :)

  integer :: alloc_err

  if (allocated(rarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(rarray(dims(1), dims(2), dims(3), dims(4), dims(5)), &
      stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocR5D

!------------------------------------------------------------------------------

subroutine CheckedAllocR6D(dims, label, rarray)

  ! Allocate a 6D real array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   rarray - the allocated real array.

  integer, intent(in) :: dims(6)
  character(len=max_string_size), intent(in) :: label
  real(dp), intent(inout), allocatable :: rarray(:, :, :, :, :, :)

  integer :: alloc_err

  if (allocated(rarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(rarray(dims(1), dims(2), dims(3), dims(4), dims(5), dims(6)), &
      stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocR6D

!------------------------------------------------------------------------------

subroutine CheckedAllocR7D(dims, label, rarray)

  ! Allocate a 7D real array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   rarray - the allocated real array.

  integer, intent(in) :: dims(7)
  character(len=max_string_size), intent(in) :: label
  real(dp), intent(inout), allocatable :: rarray(:, :, :, :, :, :, :)

  integer :: alloc_err

  if (allocated(rarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(rarray(dims(1), dims(2), dims(3), dims(4), dims(5), dims(6), &
      dims(7)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocR7D

!------------------------------------------------------------------------------

subroutine CheckedAllocI1D(dims, label, iarray)

  ! Allocate a 1D integer array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   iarray - the allocated integer array.

  integer, intent(in) :: dims(1)
  character(len=max_string_size), intent(in) :: label
  integer, intent(inout), allocatable :: iarray(:)

  integer :: alloc_err

  if (allocated(iarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(iarray(dims(1)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocI1D

!------------------------------------------------------------------------------

subroutine CheckedAllocI2D(dims, label, iarray)

  ! Allocate a 2D integer array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   iarray - the allocated integer array.

  integer, intent(in) :: dims(2)
  character(len=max_string_size), intent(in) :: label
  integer, intent(inout), allocatable :: iarray(:, :)

  integer :: alloc_err

  if (allocated(iarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(iarray(dims(1), dims(2)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocI2D

!------------------------------------------------------------------------------

subroutine CheckedAllocI3D(dims, label, iarray)

  ! Allocate a 3D integer array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   iarray - the allocated integer array.

  integer, intent(in) :: dims(3)
  character(len=max_string_size), intent(in) :: label
  integer, intent(inout), allocatable :: iarray(:, :, :)

  integer :: alloc_err

  if (allocated(iarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(iarray(dims(1), dims(2), dims(3)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocI3D

!------------------------------------------------------------------------------

subroutine CheckedAllocI4D(dims, label, iarray)

  ! Allocate a 4D integer array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   iarray - the allocated integer array.

  integer, intent(in) :: dims(4)
  character(len=max_string_size), intent(in) :: label
  integer, intent(inout), allocatable :: iarray(:, :, :, :)

  integer :: alloc_err

  if (allocated(iarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(iarray(dims(1), dims(2), dims(3), dims(4)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocI4D

!------------------------------------------------------------------------------

subroutine CheckedAllocI5D(dims, label, iarray)

  ! Allocate a 5D integer array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   iarray - the allocated integer array.

  integer, intent(in) :: dims(5)
  character(len=max_string_size), intent(in) :: label
  integer, intent(inout), allocatable :: iarray(:, :, :, :, :)

  integer :: alloc_err

  if (allocated(iarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(iarray(dims(1), dims(2), dims(3), dims(4), dims(5)), &
      stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocI5D

!------------------------------------------------------------------------------

subroutine CheckedAllocI6D(dims, label, iarray)

  ! Allocate a 6D integer array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   iarray - the allocated integer array.

  integer, intent(in) :: dims(6)
  character(len=max_string_size), intent(in) :: label
  integer, intent(inout), allocatable :: iarray(:, :, :, :, :, :)

  integer :: alloc_err

  if (allocated(iarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(iarray(dims(1), dims(2), dims(3), dims(4), dims(5), dims(6)), &
      stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocI6D

!------------------------------------------------------------------------------

subroutine CheckedAllocI7D(dims, label, iarray)

  ! Allocate a 7D integer array.

  ! In:
  !   dims - integer array of dimensions.
  ! Out:
  !   iarray - the allocated integer array.

  integer, intent(in) :: dims(7)
  character(len=max_string_size), intent(in) :: label
  integer, intent(inout), allocatable :: iarray(:, :, :, :, :, :, :)

  integer :: alloc_err

  if (allocated(iarray)) then
    write(stderr, *) "Error - already allocated: ", label
    error stop array_error_code
  endif

  allocate(iarray(dims(1), dims(2), dims(3), dims(4), dims(5), dims(6), &
      dims(7)), stat=alloc_err)

  if (alloc_err /= 0) then
    write(stderr, *) "Error allocating array ", label
    error stop array_error_code
  endif

end subroutine CheckedAllocI7D

!------------------------------------------------------------------------------

end module alloc
