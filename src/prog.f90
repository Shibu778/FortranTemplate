program prog

! Sample program code.

!------------------------------------------------------------------------------

  use consts
  use io, only : ParseCommandLine, CheckReadable
  use alloc, only : CheckedAlloc

  implicit none

!------------------------------------------------------------------------------

  integer :: dims2(2)
  character(len=max_string_size) :: label
  real(dp), allocatable :: test_real_array(:, :)
  character(len=:), allocatable :: infile

  ! Take infile as an argument to the code, and test that it's readable.
  call ParseCommandLine(infile)
  call CheckReadable(infile)

  ! To use the checked allocation routine, make an array with the extent of
  ! each dimension. And write some label text that can be used in the error
  ! message if something goes wrong.
  dims2 = [3, 3]
  write(label, *) "test_real_array"
  call CheckedAlloc(dims2, label, test_real_array)

!------------------------------------------------------------------------------

end program prog
