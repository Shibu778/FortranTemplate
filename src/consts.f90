module consts

! Declaration of various constants used in the code.

!------------------------------------------------------------------------------

  ! Use Fortran2003 io units, and double precision.
  use, intrinsic :: iso_fortran_env, only : &
      stdin => input_unit, &
      stdout => output_unit, &
      stderr => error_unit, &
      dp => real64

  implicit none

  integer, parameter :: exec_error_code = 10
  integer, parameter :: file_error_code = 11
  integer, parameter :: array_error_code = 12
  integer, parameter :: max_string_size = 256
  real(dp), parameter :: pi = 3.14159265358979323846264338_dp

!------------------------------------------------------------------------------

end module consts
